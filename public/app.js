const defaultTodos = [
  { text: 'today', completed: false },
  { text: 'tomorrow', completed: false },
  { text: 'the day after tomorrow', completed: true },
]

const todos = JSON.parse(localStorage.getItem('todos')) || defaultTodos

const elements = {
  todoList: document.getElementById('todo-list'),
  todoInput: document.getElementById('todo-input'),
  addButton: document.getElementById('add-todo')
  // resetButton: document.getElementById('reset-todo'),
}

///////////////////////////////////////////////////////////////////////////////

function renderTodoList() {
  elements.todoList.innerHTML = null
  todos.forEach((todo, index) => {
    const newTodo = document.createElement('p')
    const number = index + 1
    newTodo.innerText = `${number}.  ${todo.text}`
    newTodo.classList.add('.p')

    if (todo.completed) {
      newTodo.innerText = `${number}.  ${todo.text}   (selesai)`
    } else {

      const completeButton = document.createElement('button')
      completeButton.innerText = 'Selesai dilakukan'
      completeButton.classList.add('buttonMark')
      completeButton.addEventListener('click', () => completeTodo(index))
      newTodo.append('  ', completeButton)
      // document.querySelector('#todos').appendChild(completeButton)
    }

    const deleteButton = document.createElement('button')
    deleteButton.innerText = 'Hapus'
    deleteButton.classList.add('buttonMark')
    deleteButton.addEventListener('click', () => deleteTodo(index))
    newTodo.append('  ', deleteButton)
    // document.querySelector('#todos').appendChild(deleteButton)

    elements.todoList.append(newTodo)
  })
}

function storeAndRender() {
  localStorage.setItem('todos', JSON.stringify(todos))
  renderTodoList()
}

function isInputFilled() {
  return elements.todoInput.value.length > 0
}

function addTodo() {
  if (isInputFilled()) {
    const todoText = elements.todoInput.value
    todos.push({ text: todoText, completed: false })
    storeAndRender()
    elements.todoInput.value = ''
    elements.todoInput.focus()
  }
}

function completeTodo(index) {
  todos[index].completed = true
  storeAndRender()
}

function deleteTodo(index) {
  todos.splice(index, 1)
  storeAndRender()
}

///////////////////////////////////////////////////////////////////////////////

elements.todoInput.addEventListener('keypress', e =>
  e.keyCode === 13 ? addTodo() : {}
)

elements.addButton.addEventListener('click', () => addTodo())

// elements.resetButton.addEventListener('click', () => {
//   localStorage.clear()
//   window.location.reload()
// })

///////////////////////////////////////////////////////////////////////////////
const summaryTodo = function(incompletedTodos) {
    const summary = document.createElement('h2')
    summary.textContent = `Sisa todo ${incompletedTodos.length} lagi`

    return summary
}

storeAndRender()
elements.todoInput.focus()